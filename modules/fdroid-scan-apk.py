#!/usr/bin/env python3
#
# issuebot_apt_install = python3-jinja2
# issuebot_apt_install = dexdump

import inspect
import jinja2
import os
import re
import sys

from androguard.misc import AnalyzeAPK
from fdroidserver import update

localmodule = os.path.realpath(
    os.path.join(os.path.dirname(inspect.getfile(inspect.currentframe())), '..')
)
if localmodule not in sys.path:
    sys.path.insert(0, localmodule)
from issuebot import IssuebotModule


NON_FREE_PERMISSION_SIGNATURES = (
    r'android\.vending\.',
    r'com\.amazon\.',
    r'com\.android\.vending\.',
    r'com\.google\.',
    r'com\.huawei\.',
    r'com\.oneplus\.',
    r'com\.oppo\.',
    r'com\.samsung\.',
    r'com\.sony\.',
    r'com\.sonyericsson\.',
    r'com\.sonymobile\.',
    r'com\.spotify\.',
    r'oppo\.',
)
NON_FREE_PERMISSION_SIGNATURES = [re.compile(r) for r in NON_FREE_PERMISSION_SIGNATURES]

TRACKING_PERMISSION_SIGNATURES = (
    r'android\.permission\.ACCESS_BACKGROUND_LOCATION',
    r'android\.permission\.ACCESS_COARSE_LOCATION',
    r'android\.permission\.ACCESS_FINE_LOCATION',
    r'android\.permission\.LOADER_USAGE_STATS',
    r'android\.permission\.PACKAGE_USAGE_STATS',
    r'android\.permission\.QUERY_ALL_PACKAGES',
    r'android\.permission\.READ_ASSISTANT_APP_SEARCH_DATA',
    r'android\.permission\.READ_PHONE_NUMBERS',
    r'com\.google\.android\.c2dm\.permission\.RECEIVE',
    r'com\.google\.android\.finsky\.permission\.BIND_GET_INSTALL_REFERRER_SERVICE',
    r'com\.google\.android\.gms\.',
)
TRACKING_PERMISSION_SIGNATURES = [re.compile(r) for r in TRACKING_PERMISSION_SIGNATURES]

J2_TEMPLATE = """
<h3>Scan APK</h3>
{%- for apkfile, sections in apk_tdata.items() -%}
<details {% if apkfile in found_anti_features %}open{% endif %}><summary>{{ apkfile }}</summary><table>
{%- for perm_type, entries in sections.items() -%}
{%- if entries -%}
<tr><th><code>{{ perm_type }}</code></th><th></th></tr>
{%- for name, anti_features, matches in entries -%}
{%- if name.startswith('android.permission.') -%}
{% set display = '<a href="https://developer.android.com/reference/android/Manifest.permission#{}">{}</a>'.format(name.split('.')[2], name) %}
{%- else -%}
{% set display = name %}
{%- endif -%}
{%- if anti_features -%}
<tr><td><tt>{{ display }}</tt></td><td>🚩&nbsp;
{%- if matches -%}
{%- for url, patterns in matches -%}
<a href="{{ url }}"><abbr title="{{ patterns }}">{{ anti_features }}(ε)</abbr></a>
{%- endfor -%}
{%- else -%}
{{ anti_features }}
{%- endif -%}
</td></tr>
{%- else -%}
<tr><td colspan="2"><tt>{{ display }}</tt></td></tr>
{%- endif -%}
{%- endfor -%}
{{ extra }}
{%- endif -%}
{%- endfor -%}
</table></details>
{%- endfor -%}
"""


class FDroidScanApk(IssuebotModule):
    def get_permissions_anti_features(self, name):
        anti_features = set()
        for pat in NON_FREE_PERMISSION_SIGNATURES:
            if pat.match(name):
                self.add_label('non-free')
                anti_features.add('NonFreeDep')
                break
        for pat in TRACKING_PERMISSION_SIGNATURES:
            if pat.match(name):
                self.add_label('trackers')
                anti_features.add('Tracking')
                break
        return ','.join(sorted(anti_features)), []

    def get_code_anti_features(self, name):
        anti_features = set()
        if self.non_free_code_match(name):
            self.add_label('non-free')
            anti_features.add('NonFree')
        matches = self.etip_code_search(name)
        if matches:
            self.add_label('trackers')
            anti_features.add('Tracking')
        return ','.join(sorted(anti_features)), matches

    def get_meta_data_anti_features(self, name):
        anti_features = set()
        all_matches = set()
        if self.non_free_code_match(name):
            self.add_label('non-free')
            anti_features.add('NonFree')
        matches = self.etip_code_search(name)
        if matches:
            self.add_label('trackers')
            anti_features.add('Tracking')
            all_matches.update(matches)
        matches = self.etip_api_key_id_search(name)
        if matches:
            self.add_label('trackers')
            anti_features.add('Tracking')
            all_matches.update(matches)
        return ','.join(sorted(anti_features)), sorted(all_matches)

    def get_application_meta_data(self, apk):
        """
        Return the android:name attribute of <meta-data> entries in <application>.

        :rtype: a list of str
        """
        return list(apk.get_all_attribute_value("meta-data", "name"))

    def scan_manifest(self, apk):
        """Take an androguard APK instance and output key bits from the manifest"""
        return {
            'activity': apk.get_activities(),
            'meta-data': self.get_application_meta_data(apk),
            'provider': apk.get_providers(),
            'receiver': apk.get_receivers(),
            'service': apk.get_services(),
        }

    def main(self):
        print('FDroidScanApk', self.application_id, self.apkfiles)
        if not self.apkfiles_required():
            return

        env = jinja2.Environment(autoescape=False)  # nosec
        template = env.from_string(J2_TEMPLATE)
        apk_tdata = dict()
        found_anti_features = set()
        for apkfile in self.apkfiles:
            sections = {
                'targetSdkVersion': [],
                'uses-permission': [],
                'uses-permission-sdk-23': [],
            }
            apk_basename = os.path.basename(apkfile)
            apk_tdata[apk_basename] = sections
            report_data = update.scan_apk(apkfile, require_signature=False)
            for k in ('uses-permission', 'uses-permission-sdk-23'):
                for p in report_data.get(k, []):
                    name = p[0]
                    anti_features, matches = self.get_permissions_anti_features(name)
                    sections[k].append([name, anti_features, matches])
                    if anti_features:
                        found_anti_features.add(apk_basename)

            targetSdkVersion = report_data.get('targetSdkVersion', 0)
            if targetSdkVersion < 30:
                sections['targetSdkVersion'].append(
                    [
                        '<p>This APK targets an older Android SDK version. This app should'
                        ' be updated to a recent SDK version to gain stronger security and'
                        ' privacy protections, as long as this app does not need any of the'
                        ' features removed by targeting newer SDKs.</p>',
                        None,
                        [],
                    ]
                )
            if targetSdkVersion < 23:
                found_anti_features.add(apk_basename)
                sections['targetSdkVersion'].append(
                    [
                        '<p>🚩&nbsp;As of Android 10 (API level 29), users see a warning when they'
                        ' start an app for the first time if the app targets Android 5.1'
                        ' (API level 22) or lower.</p>',
                        None,
                        [],
                    ]
                )

            apk = AnalyzeAPK(apkfile)[0]  # 0 means dalvikVMFormats and dex are ignored

            for k, v in self.scan_manifest(apk).items():
                report_data[k] = v
                for i in v:
                    if k == 'meta-data':
                        anti_features, matches = self.get_meta_data_anti_features(i)
                    else:
                        anti_features, matches = self.get_code_anti_features(i)
                    if k not in sections:
                        sections[k] = []
                    sections[k].append([i, anti_features, matches])
                    if anti_features:
                        found_anti_features.add(apk_basename)

            for url in self.get_urls_from_apk(apk):
                matches = self.etip_network_search(url)
                k = 'URLs in classes.dex and resources.arsc'
                if k not in sections:
                    sections[k] = []
                sections[k].append([url, 'Tracking', matches])
                if matches:
                    found_anti_features.add(apk_basename)
                report_data[k] = sections[k]

            self.reply['reportData'][apk_basename] = report_data

        self.reply['report'] = template.render(
            apk_tdata=apk_tdata, found_anti_features=found_anti_features
        )
        self.write_json()


if __name__ == '__main__':
    FDroidScanApk().main()
