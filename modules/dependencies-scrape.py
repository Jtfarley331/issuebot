#!/usr/bin/env python3

import inspect
import os
import re
import sys


localmodule = os.path.realpath(
    os.path.join(os.path.dirname(inspect.getfile(inspect.currentframe())), '..')
)
if localmodule not in sys.path:
    sys.path.insert(0, localmodule)
from issuebot import IssuebotModule
from fdroidserver import common, metadata, scanner


class DependenciesScrape(IssuebotModule):
    def main(self):
        build = metadata.Build()
        build.gradle = ['([a-z0-9]*)']  # use a pattern to match all flavors
        dependency_regexs = []
        for regex in scanner.get_gradle_compile_commands(build):
            dependency_regexs.append(
                re.compile(
                    r'^' + regex.pattern + r"""\s*\(?['"](\S+)['"]\)?.*""",
                    re.IGNORECASE,
                )
            )

        paths = common.get_all_gradle_and_manifests(self.source_dir)
        output = dict()
        trackers = []
        non_free = []
        for path in paths:
            path = str(path)
            if path.endswith('AndroidManifest.xml'):
                continue
            current_file = os.path.relpath(path, self.source_dir)
            if current_file not in output:
                output[current_file] = []
            with open(path) as fp:
                linenum = 0
                for line in fp.readlines():
                    linenum += 1
                    for regex in dependency_regexs:
                        for m in regex.finditer(line):
                            if m.lastindex == 1:
                                gradle_line = m.group(1)
                                flavor = ''
                            else:
                                gradle_line = m.group(2)
                                flavor = m.group(1)
                            anti_features = set()
                            if self.non_free_code_match(gradle_line):
                                self.add_label('non-free')
                                anti_features.add('NonFree')
                                non_free.append(
                                    (gradle_line, current_file, linenum, flavor)
                                )
                            if self.etip_code_search(gradle_line):
                                self.add_label('trackers')
                                anti_features.add('Tracker')
                                trackers.append(
                                    (gradle_line, current_file, linenum, flavor)
                                )
                            output[current_file].append(
                                (
                                    gradle_line,
                                    linenum,
                                    flavor,
                                    ','.join(sorted(anti_features)),
                                )
                            )
        if output:
            self.reply['reportData']['perFile'] = output
        self.reply['reportData']['problematicEntries'] = dict()
        if non_free:
            self.reply['reportData']['problematicEntries']['non-free'] = non_free
        if trackers:
            self.reply['reportData']['problematicEntries']['tracker'] = trackers

        report = ''
        for f, libraries in sorted(
            output.items(), key=lambda i: (len(i[0].split('/')), i[0])
        ):
            url = self.get_source_url(f)
            fileentry = ''
            d_o = ''
            found_flavor = False
            found_anti_features = False
            for lib, linenum, flavor, anti_features in sorted(libraries):
                flag = ''
                if flavor:
                    found_flavor = True
                if anti_features:
                    d_o = ' open'
                    flag = '🚩&nbsp;'
                    found_anti_features = True
                if anti_features and flavor:
                    htmlline = """<tr><td><a href="{url}"><code>{lib}</code></a></td><td><tt>{flavor}</tt></td><td>{flag}{anti_features}</td></tr>\n"""
                elif anti_features:
                    htmlline = """<tr><td colspan="2"><a href="{url}"><code>{lib}</code></a></td><!-- {flavor} --><td>{flag}{anti_features}</td></tr>\n"""
                else:
                    htmlline = """<tr><td colspan="3"><a href="{url}"><code>{lib}</code></a></td><!-- {flavor} --><!-- {flag}{anti_features} --></tr>\n"""
                fileentry += htmlline.format(
                    anti_features=anti_features,
                    flag=flag,
                    flavor=flavor,
                    lib=lib,
                    url=url + '#L' + str(linenum),
                )
            if fileentry:
                report += '<details%s><summary><tt>%s</tt></summary><table>' % (d_o, f)
                if found_anti_features and found_flavor:
                    report += '<tr><th><u>dependency</u></th><th><u>gradle flavor</u></th><th><u>Anti-Features</u></th></tr>'
                elif found_anti_features:
                    report += '<tr><th colspan="2"><u>dependency</u></th><th><u>Anti-Features</u></th></tr>'
                else:
                    report += '<tr><th colspan="3"><u>dependency</u></th></tr>'
                report += fileentry
                report += '</table></details>'
        if report:
            self.reply['report'] = (
                self.get_source_scanning_header(
                    'Dependencies scraped from gradle files'
                )
                + report
            )
        self.write_json()


if __name__ == "__main__":
    DependenciesScrape().main()
